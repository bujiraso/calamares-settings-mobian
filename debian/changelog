calamares-settings-mobian (0.2.3) unstable; urgency=medium

  * script: drop lsblk for boot disk search

 -- undef <debian@undef.tools>  Thu, 26 May 2022 04:35:21 +0000

calamares-settings-mobian (0.2.2) unstable; urgency=medium

  * scripts: Drop u-boot install on sunxi64 devices

 -- undef <debian@undef.tools>  Sat, 02 Apr 2022 12:48:26 +0000

calamares-settings-mobian (0.2.1) unstable; urgency=medium

  * scripts/c-i-b: regenerate *ramfs after changing fstab
  * scripts/c-m-i: also filter zram devices

 -- undef <debian@undef.tools>  Tue, 29 Mar 2022 09:34:10 +0000

calamares-settings-mobian (0.2.0) unstable; urgency=medium

  [ undef ]
  * scripts/c-i-b: generate miniramfs configs when miniramfs present.
    This should detect the use of miniramfs and automatically add the
    required mobian.boot parameters. Additionally, it will generate the
    miniramfs files for use during boot.
  * scripts/c-i-b: remove miniramfs on PPP of not encrypted.
    This reduces the complexity of booting the PPP without encryption. The
    rest of the script checks for miniramfs before configuring it, leaving
    the system working as it did on the OG PP.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Mon, 28 Mar 2022 13:27:36 +0200

calamares-settings-mobian (0.1.9) unstable; urgency=medium

  * scripts: Only install u-boot on allwinner devices

 -- undef <debian@undef.tools>  Mon, 07 Mar 2022 23:42:15 +0000

calamares-settings-mobian (0.1.8) unstable; urgency=medium

  [ Undef ]
  * d/changelog: Fix typo in suite
  * config: swap f2fs for BTRFS

  [ Nathan Schulte ]
  * use device tree model as device name; support PPP
  * overhaul calamares-mobian-ondev
  * improve lintian-overrides comment
  * improve installer partition sanity check message
  * use margin for keyboard character preview

 -- Undef <debian@undef.tools>  Sun, 06 Mar 2022 02:47:12 +0000

calamares-settings-mobian (0.1.7) unstable; urgency=medium

  * Ensure squashfs kernel module is loaded on boot

 -- undef <debian@undef.tools>  Mon, 06 Dec 2021 08:35:42 +0000

calamares-settings-mobian (0.1.6) unstable; urgency=medium

  * Mobile config: default to ext4 root FS

 -- Undef <debian@undef.tools>  Thu, 02 Dec 2021 07:40:41 +0000

calamares-settings-mobian (0.1.5) unstable; urgency=medium

  * prepare-internal-storage: fix bootloader installation.
  * Indent more consistently.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Wed, 03 Nov 2021 10:50:41 +0100

calamares-settings-mobian (0.1.4) unstable; urgency=medium

  [ undef ]
  * config: Update welcome screen to Bookworm
  * scripts: Update parent_blk for latest lsblk output

  [ Arnaud Ferraris ]
  * d/gbp.conf: use mobian parameters

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 01 Nov 2021 14:21:38 +0100

calamares-settings-mobian (0.1.3) unstable; urgency=medium

  [ undef ]
  * bugfix: Don't set internal target when booting from eMMC.
    This caused the installer to prompt to install from SD card to eMMC
    when booting from the eMMC, resulting in a failed install.
    Thanks to @jpnc for finding this.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 15 Mar 2021 11:36:32 +0100

calamares-settings-mobian (0.1.2) unstable; urgency=medium

  * calamares-install-bootloader: fix shutdown in the SD -> eMMC case

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sun, 14 Mar 2021 19:19:14 +0100

calamares-settings-mobian (0.1.1) unstable; urgency=medium

  [ undef ]
  * replace shellprocess reboot with conditional shutdown/reboot.
    Rather than just rebooting at the end of the install, we should shutdown
    if installing from SD to eMMC or similar. To do this, the shutdown
    command needs to be moved from shellprocess.conf to
    calamares-install-bootloader where the conditional logic can be applied.
  * Fixup: shutdown on SD -> eMMC.
    These were backwards.
  * c-install-boot: bugfix block device is dm-0 on unstable.
    Unstable changes the block device of encrypted partitions to dm-0,
    rather than /dev/mmcblkXpY. This confuses the existing script, causing
    SD -> SD installs to be considered SD -> eMMC and results in the eMMC
    install being corrupted and the SD /boot not being updated.
  * workaround: Remove BTRFS option.
    This avoids a bug in the partition UUID code triggered by unencrypted
    BTRFS installs.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 13 Mar 2021 14:24:29 +0100

calamares-settings-mobian (0.1.0) unstable; urgency=medium

  [ undef ]
  * Split settings package from calamares-extensions.
    This enables the Mobian specific configuration to be separated from the
    upstream calamares-extensions package.
  * d/* Cleanup from package split
  * Move installed files out of package directory.
    This was a layover from when calamares-settings-mobile was also a
    package for calamares extensions.
  * d/copyright: Fix for split package.
    Remove copyright allocation of calamares code (no longer in this
    package)
    Include files moved out of debian/* in copyright.
  * Add initial changes required for new upstream fs selection feature
  * Add label and force to BTRFS filesystem command.
    This aligns the command with f2fs and ext4, both of which have the label
    root and force filesystem creation.
    The latter of these is more useful with the external -> internal version
    of the installer, which creates an ext4 partition before PartitionJob
    attempts to create the user selected partition.
  * Add External -> Internal storage handling.
    This allows the mobian installer to take advantage of the calamares
    feature to allow installing to eMMC from the SD card.
    CURRENT STATUS: Works on pinephone only.
  * Correct boot entry in fstab for SD->eMMC installs.
    The SD card was being added to fstab on SD -> eMMC installs. This
    resulted in the system failing to boot.
    This commit deletes the original entry and replaces it with the correct
    UUID of the target drives /boot.

  [ Arnaud Ferraris ]
  * Remove all files from calamares-extensions.
    This prepares the package split
  * Move all scripts to a dedicated folder
  * d/source: remove now-obsolete include-binaries
  * debian: cleanup and fixes following package split

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 11 Mar 2021 17:39:59 +0100

calamares-settings-mobian (0.0.4-3) unstable; urgency=medium

  * calamares-install-bootloader: ensure kernel partition table is updated

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 17 Feb 2021 01:09:11 +0100

calamares-settings-mobian (0.0.4-2) unstable; urgency=medium

  [ Undef ]
  * Modify UUID of /boot on start of calamares.
    Should resolve https://gitlab.com/mobian1/issues/-/issues/235, but is currently. untested.
    The issue occurs because all images have the same UUID. The installer will mount the eMMC /boot as the SD cards /boot and overwrite the configuration the eMMC version.
  * Fix UUIDs in fstab
  * Unmount /boot before changing it's uuid.
    Without unmounting, the UUID of /boot cannot be fixed.
    also adds end quotes to the two UUID varibales.
  * Add pause before trying to get new UUID.
    Without this, we get the old UUID back, which results in any reboot of the installer failing.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 16 Feb 2021 10:30:08 +0100

calamares-settings-mobian (0.0.4-1) unstable; urgency=medium

  [ undef ]
  * d/control: Remove unnecessary dependencies.
  * Rebase package to calamares-extensions upstream
  * d/patches: update module patch for new upstream.
    Changes are now rebased on current calamares-extensions, which required
    minor modifications to the module patch.
  * d/* reshuffle file locations for more efficient install
    d/branding created to contain a mobian branding rather than patching
     the default-mobile branding. Note, this is still installed as default-mobile
     as the mobile module's QML hard-codes this name.
    d/config moved module config to d/config/modules and fixed relevant install line.
    d/control remove superfluous quilt build-depend.
    d/patches Remove branding (see d/branding) and limit-modules patch.
     The latter was unnecessary.
    d/calamares-settings-mobian.service renamed to automate install.
  * d/copyright correct upstream contributors.
    Previous changelog was taken from osk-sdl. This one matches
    calamares-extensions.
  * d/patches add wait-time-wording patch.
    Fixes the wording of wait.qml to ensure users don't believe the installer
    has hung.
  * d/copyright fix source URL.
    Source URL should point to upstream source, not package VCS.

  [ Arnaud Ferraris ]
  * debian: don't install default branding.
    This would cause conflicts with `calamares` which installs the same
    files.
  * debian: don't use dh-exec.
    Current debhelper seems to not need it.
  * d/patches: remove PartitionJob patch
  * calamares-install-bootloader: use correct luks volume name.
    The encrypted partition label is now `calamares_crypt` due to upstream
    changes, let's use it for our crypttab setup.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 13 Jan 2021 15:55:36 +0100

calamares-settings-mobian (0.0.3) unstable; urgency=medium

  * calamaresfb: use 'Fusion' theme for a better look
  * d/control: depend on cryptsetup

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 15 Dec 2020 12:16:39 +0100

calamares-settings-mobian (0.0.2) unstable; urgency=medium

  * Revert "Use Plasma UI in Calamares"

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 02 Dec 2020 14:30:17 +0100

calamares-settings-mobian (0.0.1) unstable; urgency=medium

  [ undef ]
  * Initial release

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 01 Dec 2020 11:13:48 +0100
